/**
 * @param {number} numRows
 * @return {number[][]}
 */
var generate = function (numRows) {
  const output = [];
  for (let i = 0; i < numRows; i++) {
    const lastOutput = output[output.length - 1];
    if (!lastOutput) {
      output.push([1]);
    } else {
      const newOutput = Array(lastOutput.length + 1);
      for (let j = 0; j < newOutput.length; j++) {
        if (j === 0) {
          newOutput[0] = 1;
        } else if (j === newOutput.length - 1) {
          newOutput[newOutput.length - 1] = 1;
        } else {
          newOutput[j] = lastOutput[j] + lastOutput[j - 1];
        }
      }
      output.push(newOutput);
    }
  }
  return output;
};

generate(7);