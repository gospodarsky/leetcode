/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} targetSum
 * @return {boolean}
 */
const hasPathSum = function (root, targetSum) {
  function traverse(root, acc, reached) {
    acc += root.val;
    if (acc === targetSum && !root.left && !root.right) {
      return true;
    }
    if (root.left && !reached) {
      reached = traverse(root.left, acc, reached);
    }
    if (root.right && !reached) {
      reached = traverse(root.right, acc, reached);
    }
    return reached;
  }

  return root ? traverse(root, null, false) : false;
};