// Input: head = [1,2,3,4,5], n = 2
// Output: [1,2,3,5]

// Input: head = [1], n = 1
// Output: []

// Input: head = [1,2], n = 1
// Output: [1]

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function (head, n) {
  // Pointers
  let start = head;
  let end = head;

  while (head.next) {
    head.next.prev = head;
    head = head.next;
    end = head;
  }

  while (n > 1) {
    end = end.prev;
    n--;
  }

  if (!end.prev && !end.next) {
    return null;
  } else if (!end.prev) {
    start = end.next;
  } else if (!end.next) {
    end.prev.next = null;
  } else {
    end.prev.next = end.next;
    end.next.prev = end.prev;
  }

  return start;
};