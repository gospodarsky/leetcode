/**
 * @param {string} s
 * @return {number}
 */
const firstUniqChar = function (s) {
  const map = new Map();
  let firstUniqCharIndex = -1;
  for (let i = 0; i < s.length; i++) {
    const char = s[i];
    const previousCount = map.get(char) || 0;
    map.set(char, previousCount + 1);
  }
  const iterator = map.values();
  for (let [key, value] of map) {
    if (value === 1) {
      firstUniqCharIndex = s.indexOf(key);
      break;
    }
  }
  return firstUniqCharIndex;
};

firstUniqChar("loveleetcode");