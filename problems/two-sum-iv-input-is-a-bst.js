/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} k
 * @return {boolean}
 */
var findTarget = function (root, k) {
  function traverse(root, state) {
    if (root === null) {
      return false;
    }
    if (state.get(k - root.val) !== undefined) {
      return true;
    } else {
      state.set(root.val, k - root.val);
    }
    return traverse(root.left, state) || traverse(root.right, state);
  }

  return traverse(root, new Map());
};