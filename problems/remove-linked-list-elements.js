/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} val
 * @return {ListNode}
 */
const removeElements2 = function (head, val) {
  const start = new ListNode();
  let prev = start;
  while (head) {
    if (head.val !== val) {
      prev.next = head;
      head = head.next;
      prev = prev.next;
    } else {
      head = head.next;
      prev.next = null;
    }
  }
  return start.next;
};

const removeElements = function (head, val) {
  if (!head) {
    return null;
  }
  
  head.next = removeElements(head.next, val);

  if (head.val === val) {
    return head.next;
  } else {
    return head;
  }
};