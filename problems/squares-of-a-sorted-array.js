// Example 1:
// Input: nums = [-4,-1,0,3,10]
// Output: [0,1,9,16,100]

// Example 2:
// Input: nums = [-7,-3,2,3,11]
// Output: [4,9,9,49,121]

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortedSquares = function (nums) {
  return nums.map(x => x * x).sort((a, b) => a - b);
};

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortedSquares2 = function (nums) {
  const res = Array(nums.length);
  let left = 0;
  let right = nums.length - 1;
  let square;
  for (let i = nums.length - 1; i >= 0; i--) {
    if (Math.abs(nums[left]) > Math.abs(nums[right])) {
      square = nums[left] * nums[left];
      left++;
    } else {
      square = nums[right] * nums[right];
      right--;
    }
    res[i] = square;
  }
  return res;
};