/**
 * @param {number[][]} grid
 * @return {number}
 */
const maxAreaOfIsland = function (grid) {
  let islandArea = 0;
  let maxIslandArea = 0;
  let sqrt = grid.length * grid[0].length;
  const stack = [];

  const deepFirstSearch = function (grid, r, c, resetIsland, from) {
    if (sqrt) {
      if (grid[r][c] === 0) {
        grid[r][c] = 2;
        sqrt = sqrt - 1;

        if (resetIsland) {
          islandArea = 0;
        }

        if (r + 1 < grid.length) {
          deepFirstSearch(grid, r + 1, c, true, '');
        }
        if (c + 1 < grid[0].length) {
          deepFirstSearch(grid, r, c + 1, true, '');
        }
      } else if (grid[r][c] === 1) {
        grid[r][c] = 3;
        sqrt = sqrt - 1;

        if (!from) {
          islandArea = 0;
        }

        islandArea++;

        if (maxIslandArea < islandArea) {
          maxIslandArea = islandArea;
        }

        if (grid[r - 1] !== undefined && grid[r - 1][c] === 1) {
          deepFirstSearch(grid, r - 1, c, false, 'S');
        }
        if (grid[r][c + 1] === 1) {
          deepFirstSearch(grid, r, c + 1, false, 'W');
        }
        if (grid[r + 1] !== undefined && grid[r + 1][c] === 1) {
          deepFirstSearch(grid, r + 1, c, false, 'N');
        }
        if (grid[r][c - 1] === 1) {
          deepFirstSearch(grid, r, c - 1, false, 'E');
        }
        if (grid[r - 1] !== undefined && grid[r - 1][c] === 0) {
          stack.push([grid, r - 1, c, true, '']);
        }
        if (grid[r][c + 1] === 0) {
          stack.push([grid, r, c + 1, true, '']);
        }
        if (grid[r + 1] !== undefined && grid[r + 1][c] === 0) {
          stack.push([grid, r + 1, c, true, '']);
        }
        if (grid[r][c - 1] === 0) {
          stack.push([grid, r, c - 1, true, '']);
        }
      }
    }
  };

  deepFirstSearch(grid, 0, 0, false, '');

  while (stack.length) {
    deepFirstSearch(...stack.pop());
  }

  return maxIslandArea;
};

maxAreaOfIsland([
  [0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
  [0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0],
  [0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0]
]);