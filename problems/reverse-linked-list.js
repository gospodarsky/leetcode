/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
const reverseList = function (head) {
  let prev = null, tmp;

  while (head) {
    tmp = head.next;
    head.next = prev;
    prev = head;
    head = tmp;
  }

  return prev;
};

const reverseList2 = function (head) {
  if (head === null || head.next === null) {
    return head;
  }
  const p = reverseList(head.next);
  head.next.next = head;
  head.next = null;
  return p;
};