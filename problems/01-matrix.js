/**
 * @param {number[][]} mat
 * @return {number[][]}
 */
var updateMatrix = function (mat) {
  const zerosXY = [];
  const distance = [...mat];

  function findNearestZeroDistance([x, y]) {
    let nearestZeroDistance;

    zerosXY.forEach(zeroXY => {
      const xDiff = Math.abs(x - zeroXY[0]);
      const yDiff = Math.abs(y - zeroXY[1]);
      const diff = xDiff + yDiff;

      if (nearestZeroDistance === undefined) {
        nearestZeroDistance = diff;
      } else {
        nearestZeroDistance = nearestZeroDistance > diff ? diff : nearestZeroDistance;
      }
    });

    return nearestZeroDistance;
  }

  for (let i = 0; i < mat.length; i++) {
    for (let j = 0; j < mat[i].length; j++) {
      if (mat[i][j] === 0) {
        zerosXY.push([i, j]);
      }
    }
  }

  for (let i = 0; i < mat.length; i++) {
    for (let j = 0; j < mat[i].length; j++) {
      if (mat[i][j] === 0) {
        distance[i][j] = 0;
      } else {
        distance[i][j] = findNearestZeroDistance([i, j]);
      }
    }
  }

  return distance;
};
