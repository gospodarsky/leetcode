/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} val
 * @return {TreeNode}
 */
const insertIntoBST = function (root, val) {
  let head = root;

  function traverse(root, val) {
    if (root.val > val) {
      if (root.left) {
        traverse(root.left, val);
      } else {
        root.left = new TreeNode(val);
      }
    } else {
      if (root.right) {
        traverse(root.right, val);
      } else {
        root.right = new TreeNode(val);
      }
    }
  }

  if (root) {
    traverse(root, val);
  } else {
    head = new TreeNode(val);
  }

  return head;
};