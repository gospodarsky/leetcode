/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
const maxDepth = function (root) {
  let depth = 0;

  function traverse(root, index) {
    if (root) {
      index += 1;
      if (index > depth) {
        depth = index;
      }
      traverse(root.left, index);
      traverse(root.right, index);
    }
    return depth;
  }

  return traverse(root, depth);
};