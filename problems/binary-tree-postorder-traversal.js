/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
const postorderTraversal = function (root) {
  function traverse(root, values) {
    if (root) {
      traverse(root.left, values);
      traverse(root.right, values);
      values.push(root.val);
    }
    return values;
  }

  return traverse(root, []);
};