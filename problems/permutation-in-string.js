// Input: s1 = "ab", s2 = "eidbaooo"
// Output: true
// Explanation: s2 contains one permutation of s1 ("ba").

// Input: s1 = "ab", s2 = "eidboaoo"
// Output: false

var checkInclusion = function (s1, s2) {
  if (s1.length > s2.length) {
    return false;
  }
  const s1map = Array(26).fill(0);
  const s2map = Array(26).fill(0);
  for (let i = 0; i < s1.length; i++) {
    s1map[s1.charCodeAt(i) - String.prototype.charCodeAt.call('a')]++;
    s2map[s2.charCodeAt(i) - String.prototype.charCodeAt.call('a')]++;
  }
  for (let i = 0; i < s2.length - s1.length; i++) {
    if (matches(s1map, s2map)) {
      return true;
    }
    s2map[s2.charCodeAt(i + s1.length) - String.prototype.charCodeAt.call('a')]++;
    s2map[s2.charCodeAt(i) - String.prototype.charCodeAt.call('a')]--;
  }
  return matches(s1map, s2map);
};

function matches(s1map, s2map) {
  for (let i = 0; i < 26; i++) {
    if (s1map[i] !== s2map[i])
      return false;
  }
  return true;
}

console.log(checkInclusion("adc", "dcda"));