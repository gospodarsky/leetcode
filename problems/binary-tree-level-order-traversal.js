/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
const levelOrder = function (root) {
  function traverse(root, output, index) {
    if (root) {
      output[index] = output[index] || [];
      output[index].push(root.val);
      index += 1;
      traverse(root.left, output, index);
      traverse(root.right, output, index);
    }
    return output;
  }

  return traverse(root, [], 0);
};