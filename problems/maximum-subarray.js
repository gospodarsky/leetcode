/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function (nums) {
  let sum = nums[0];
  let maxSum = nums[0];
  for (let i = 1; i < nums.length; i++) {
    const el = nums[i];
    sum = Math.max(el, sum + el);
    maxSum = Math.max(maxSum, sum);
  }
  return maxSum;
};

console.log(maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]));
console.log(maxSubArray([1]));
console.log(maxSubArray([5, 4, -1, 7, 8]));
console.log(maxSubArray([-1]));
console.log(maxSubArray([-2, -1]));