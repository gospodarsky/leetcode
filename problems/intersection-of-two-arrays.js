/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
 var intersect = function(nums1, nums2) {
    const map = {};
    const res = [];
    const [big, small] = nums1.length > nums2.length ? [nums1, nums2] : [nums2, nums1];

    function fillMap(nums) {
      nums.forEach(num => {
        if (map[num] === undefined) {
          map[num] = 1;
        } else {
          map[num]++;
        }
      });
    }

    fillMap(small);

    big.forEach(num => {
      if (map[num]) {
        res.push(num);
        map[num]--;
      }
    });

    return res;
};