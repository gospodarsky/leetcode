/**
 * // Definition for a Node.
 * function Node(val, left, right, next) {
 *    this.val = val === undefined ? null : val;
 *    this.left = left === undefined ? null : left;
 *    this.right = right === undefined ? null : right;
 *    this.next = next === undefined ? null : next;
 * };
 */
//  Input: root = [1,2,3,4,5,6,7]
//  Output: [1,#,2,3,#,4,5,6,7,#]
/**
 * @param {Node} root
 * @return {Node}
 */
var connect = function (root) {
  const map = {};
  const level = 0;

  function fillStack(root, level) {
    if (!root) {
      return null;
    }

    if (map[level] === undefined) {
      map[level] = [];
    }

    map[level].push(root);

    fillStack(root.left, level + 1);
    fillStack(root.right, level + 1);
  }

  fillStack(root, level);

  const stack = Object.values(map).reduce((acc, arr) => acc.concat(arr), []);
  let skip = 0;
  
  stack.forEach((el, index, arr) => {
    if (index !== skip) {
      el.next = arr[index + 1] || null;
    } else {
      skip = !skip ? 2 : 2 + skip * 2;
      el.next = null;
    }
  });

  return root;
};
