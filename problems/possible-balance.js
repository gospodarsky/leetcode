function possibleBalance(str, n) {
  let removals = 0;

  function balance(str, n) {
    let plus = 0;
    let minus = 0;
    for (let i = 0; i < str.length; i++) {
      let element = str[i];
      element === '+' ? plus++ : minus++;
    }
    if (Math.max(minus, plus) < n) {
      return -1;
    }
    const diff = plus - minus;
    if (diff === n) {
      return removals;
    } else {
      removals++;
      return balance(str.slice(0, str.length - 1), n);
    }
  }

  return balance(str, n);
}

console.log(possibleBalance("++-", 2)); // should return 1
console.log(possibleBalance("+++-++-++--+-++++-+--++-++-+-++++-+++--", 12)); // 1
console.log(possibleBalance("+++-++-++--+-++++-+--++-++-+-++++-+++--", 13)); // 2
console.log(possibleBalance("+++-++-++--+-++++-+--++-++-+-++++-+++--", 14)); // -1
console.log(possibleBalance("+++---", 3)); // 3
console.log(possibleBalance("+++-+---", 3)); // 3
console.log(possibleBalance("----+-", -2)); // 4
