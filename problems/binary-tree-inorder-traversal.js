/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
const inorderTraversal = function (root) {
  function traverse(root, values) {
    if (root) {
      traverse(root.left, values);
      values.push(root.val);
      traverse(root.right, values);
    }
    return values;
  }

  return traverse(root, []);
};