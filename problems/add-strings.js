/**
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 */
var addStrings = function (num1, num2) {
  const maxLength = Math.max(num1.length, num2.length);
  const n1 = num1.padStart(maxLength, '0');
  const n2 = num2.padStart(maxLength, '0');

  let rest = 0;
  let result = "";
  for (let i = maxLength - 1; i >= 0; i--) {
    const digit1 = Number(n1[i]);
    const digit2 = Number(n2[i]);
    const sum = digit1 + digit2 + rest;
    if (sum > 9) {
      result = sum % 10 + result;
      rest = 1;
    } else {
      result = sum + result;
      rest = 0;
    }
  }
  if (rest) {
    result = rest + result;
  }
  return result;
};