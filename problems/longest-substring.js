// Input: s = "abcabcbb"
// Output: 3

// Input: s = "bbbbb"
// Output: 1

// Input: s = "pwwkew"
// Output: 3

/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function (s) {
  var str = '';
  var maxLength = 0;
  for (let i = 0; i < s.length; i++) {
    const char = s[i];
    if (str.includes(char)) {
      i = s.lastIndexOf(char, i - 1);
      str = '';
    } else {
      str = str + char;
    }
    maxLength = str.length > maxLength ? str.length : maxLength;
  }
  return maxLength;
};

var lengthOfLongestSubstring = function (s) {
  var left = 0;
  var right = 0;
  var length = s.length - 1;
  var res = 0;
  var set = {};

  while (right <= length) {
    var charRight = s[right];
    set[charRight] = set[charRight] ? set[charRight] + 1 : 1;

    while (set[charRight] > 1) {
      var charLeft = s[left];
      set[charLeft]--;
      left++;
    }

    right++;

    res = Math.max(res, right - left);
  }

  return res;
};