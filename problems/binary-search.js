// Example 1:
// Input: nums = [-1,0,3,5,9,12]; target = 9
// Output: 4

// Example 2:
// Input: nums = [-1,0,3,5,9,12]; target = 2
// Output: -1

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function (nums, target) {
  return nums.indexOf(target);
};

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search2 = function (nums, target) {
  let left = 0;
  let right = nums.length - 1;

  while (left <= right) {
    const index = parseInt((left + right) / 2);
    if (target === nums[index]) {
      return index;
    } else if (target < nums[index]) {
      right = index - 1;
    } else if (target > nums[index]) {
      left = index + 1;
    }
  }

  return -1;
};