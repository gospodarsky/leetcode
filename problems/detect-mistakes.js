// Constraints:
// 1 <= nums[i] <= 100
/**
 * @param {number[]} nums
 * @return {number[]}
 */
const detectMistakes = function (nums) {
  const mistakes = [];
  const arr = Array(nums.length);
  for (let i = 0; i < nums.length; i++) {
    const element = nums[i];
    if (arr[element - 1] !== undefined) {
      mistakes.push(element);
    } else {
      arr[element - 1] = element;
    }
  }
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === undefined) {
      mistakes.push(i + 1);
    }
  }
  return mistakes;
};

console.log(detectMistakes([1, 2, 2, 4, 5])); // [2,3]
console.log(detectMistakes([6, 2, 5, 2, 2])); // [1,2,2,3,4]
console.log(detectMistakes([5, 5, 4, 3, 3])); // [1,2,3,5]
console.log(detectMistakes([5, 5, 5, 5, 5])); // [1,2,3,4,5,5,5,5]