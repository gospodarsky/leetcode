/**
 * @param {number[][]} mat
 * @param {number} r
 * @param {number} c
 * @return {number[][]}
 */
var matrixReshape = function (mat, r, c) {
  if (r * c !== mat.length * mat[0].length) {
    return mat;
  }
  const reshape = Array(r).fill().map(row => []);
  for (let i = 0, k = 0, l = 0; i < mat.length; i++) {
    for (let j = 0; j < mat[i].length; j++) {
      if (l === c) {
        k++;
        l = 0;
      }
      reshape[k][l++] = mat[i][j];
    }
  }
  return reshape;
};
