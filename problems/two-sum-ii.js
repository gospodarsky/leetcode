// Input: nums = [2,7,11,15], target = 9
// Output: [1,2]

// Input: nums = [2,3,4], target = 6
// Output: [1,3]

// Input: nums = [-1,0], target = -1
// Output: [1,2]

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (nums, target) {
  for (let i = 0; i < nums.length; i++) {
    let j = i + 1;
    const length = nums.length;
    while (j < length) {
      if (nums[i] + nums[j] === target) {
        return [i + 1, j + 1];
      }
      j++;
    }
  }
};

var twoSum = function (nums, target) {
  const obj = {}
  for (let i = 0; i < nums.length; i++) {
    obj[nums[i]] = i;
  }
  for (let i = 0; i < nums.length; i++) {
    if (obj[target - nums[i]] !== undefined) {
      return [i + 1, obj[target - nums[i]] + 1];
    }
  }
};