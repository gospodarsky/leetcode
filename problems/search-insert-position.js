// Example 1:
// Input: nums = [1,3,5,6], target = 5
// Output: 2

// Example 2:
// Input: nums = [1,3,5,6], target = 2
// Output: 1

// Example 3:
// Input: nums = [1,3,5,6], target = 7
// Output: 4

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
 var searchInsert = function(nums, target) {
  let left = 0;
  let right = nums.length - 1;

  while (left <= right) {
    const index = parseInt((left + right) / 2);
    if (target === nums[index]) {
      return index;
    } else if (target < nums[index]) {
      right = index - 1;
    } else if (target > nums[index]) {
      left = index + 1;
    }
  }

  return left;
};