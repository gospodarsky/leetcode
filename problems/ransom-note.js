/**
 * @param {string} ransomNote
 * @param {string} magazine
 * @return {boolean}
 */
const canConstruct = function (ransomNote, magazine) {
  if (magazine.length < ransomNote.length) {
    return false;
  }
  const magazineMap = new Map();
  const ransomNoteMap = new Map();
  for (let i = 0; i < magazine.length; i++) {
    const char = magazine[i];
    const previousCount = magazineMap.get(char) || 0;
    magazineMap.set(char, previousCount + 1);
    if (i < ransomNote.length) {
      const char = ransomNote[i];
      const previousCount = ransomNoteMap.get(char) || 0;
      ransomNoteMap.set(char, previousCount + 1);
    }
  }
  for (let [key, value] of ransomNoteMap) {
    if (value > (magazineMap.get(key) || 0)) {
      return false;
    }
  }
  return true;
};

canConstruct("a", "b");