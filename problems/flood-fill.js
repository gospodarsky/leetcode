/**
 * With store
 * @param {number[][]} image
 * @param {number} sr
 * @param {number} sc
 * @param {number} color
 * @return {number[][]}
 */
var floodFill = function (image, sr, sc, color) {
  const stack = [[sr, sc]];
  while (stack.length) {
    const [x, y] = stack.pop();
    const target = image[x][y];
    if (target !== color) {
      if (image[x - 1] !== undefined && image[x - 1][y] === target) {
        stack.push([x - 1, y]);
      }
      if (image[x + 1] !== undefined && image[x + 1][y] === target) {
        stack.push([x + 1, y]);
      }
      if (image[x] !== undefined && image[x][y - 1] === target) {
        stack.push([x, y - 1]);
      }
      if (image[x] !== undefined && image[x][y + 1] === target) {
        stack.push([x, y + 1]);
      }
      image[x][y] = color;
    }
  }
  return image;
};

// Without store
var floodFill2 = function (image, sr, sc, newColor) {
  const color = image[sr][sc];
  if (color !== newColor) {
    dfs(image, sr, sc, color, newColor);
  }
  return image;
};

var dfs = function (image, r, c, color, newColor) {
  if (image[r][c] === color) {
    image[r][c] = newColor;
    if (r >= 1) {
      dfs(image, r - 1, c, color, newColor);
    }
    if (c >= 1) {
      dfs(image, r, c - 1, color, newColor);
    }
    if (r + 1 < image.length) {
      dfs(image, r + 1, c, color, newColor);
    }
    if (c + 1 < image[0].length) {
      dfs(image, r, c + 1, color, newColor);
    }
  }
};

floodFill([[1, 1, 1], [1, 1, 0], [1, 0, 1]], 1, 1, 2);
floodFill([[0, 0, 0], [0, 0, 0]], 0, 0, 0);
floodFill2([[1, 1, 1], [1, 1, 0], [1, 0, 1]], 1, 1, 2);
floodFill2([[0, 0, 0], [0, 0, 0]], 0, 0, 0);