/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
const searchMatrix = function (matrix, target) {
  const [m, n] = [matrix.length, matrix[0].length];
  if (target < matrix[0][0] || target > matrix[m - 1][n - 1]) {
    return false;
  }

  let left = 0, right = m * n - 1;
  let pivotIdx, pivotElement;
  while (left <= right) {
    pivotIdx = Math.floor((left + right) / 2);
    pivotElement = matrix[Math.floor(pivotIdx / n)][pivotIdx % n];
    if (target === pivotElement) {
      return true;
    } else {
      if (target < pivotElement) {
        right = pivotIdx - 1;
      } else {
        left = pivotIdx + 1;
      }
    }
  }
  return false;
};

searchMatrix([
  [1, 3, 5, 7],
  [10, 11, 16, 20],
  [23, 30, 34, 60]
], 20);

searchMatrix([
  [1],
  [3]
], 2);