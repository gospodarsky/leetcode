/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */
const hasCycle = function (head) {
  if (!head) {
    return false;
  }
  let pivot = head;
  while (pivot.next) {
    if (pivot.wasHere) {
      return true;
    } else {
      pivot.wasHere = true;
    }
    pivot = pivot.next;
  }
  return false;
};

const hasCycle2 = function (head) {
  if (!head) {
    return false;
  }
  const set = new Set();
  let pivot = head;
  while (pivot.next) {
    if (set.has(pivot)) {
      return true;
    }
    set.add(pivot);
    pivot = pivot.next;
  }
  return false;
};

const hasCycle3 = function (head) {
  if (!head) {
    return false;
  }
  let slow = head;
  let fast = head.next;
  while (slow !== fast) {
    if (fast === null || fast.next === null) {
      return false;
    }
    slow = slow.next;
    fast = fast.next.next;
  }
  return true;
};