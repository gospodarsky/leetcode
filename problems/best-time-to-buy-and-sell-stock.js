/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (prices) {
  let profit = 0;
  let buy = prices[0];
  for (let i = 1; i < prices.length; i++) {
    const todayPrice = prices[i];
    const sell = todayPrice - buy;
    if (sell > profit) {
      profit = sell;
    }
    if (todayPrice < buy) {
      buy = todayPrice;
    }
  }
  return profit;
};