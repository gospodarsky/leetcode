/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
  // End to start approach
  let i = m + n - 1;
  while (i >= 0 && (n >= 0 || m >= 0)) {
    // A wrapper to finish sorting when one of arrays has been iterated.
    if (nums2[n - 1] === undefined) {
      nums1[i] = nums1[m - 1];
      m--;
    } else if (nums1[m - 1] === undefined) {
      nums1[i] = nums2[n - 1];
      n--;
    } else {
      // Core
      if (nums1[m - 1] > nums2[n - 1]) {
        nums1[i] = nums1[m - 1];
        m--;
      } else {
        nums1[i] = nums2[n - 1];
        n--;
      }
    }
    i--;
  }
};

const arr1 = [1, 2, 3, 0, 0, 0];
const arr2 = [2, 5, 6];
merge(arr1, 3, arr2, 3);

console.log(arr1);

// Example 1:
// Input: nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
// Output: [1,2,2,3,5,6]
// Explanation: The arrays we are merging are [1,2,3] and [2,5,6].
// The result of the merge is [1,2,2,3,5,6] with the underlined elements coming from nums1.

// Example 2:
// Input: nums1 = [1], m = 1, nums2 = [], n = 0
// Output: [1]
// Explanation: The arrays we are merging are [1] and [].
// The result of the merge is [1].

// Example 3:
// Input: nums1 = [0], m = 0, nums2 = [1], n = 1
// Output: [1]
// Explanation: The arrays we are merging are [] and [1].
// The result of the merge is [1].
// Note that because m = 0, there are no elements in nums1. The 0 is only there to ensure the merge result can fit in nums1.