// Constraints:
// board.length == 9
// board[i].length == 9
// board[i][j] is a digit 1-9 or '.'.
/**
 * @param {character[][]} board
 * @return {boolean}
 */
const isValidSudoku = function (board) {
  const n = 9;

  const rows = new Set();
  const cols = new Set();
  const boxes = new Set();

  for (let r = 0; r < n; r++) {
    for (let c = 0; c < n; c++) {
      const rowEl = board[r][c];
      const colEl = board[c][r];
      const boxRowIdx = 3 * Math.floor(r / 3) + Math.floor(c / 3);
      const boxColIdx = ((r * 3) % 9) + (c % 3);
      const boxEl = board[boxRowIdx][boxColIdx];

      if (rows.has(rowEl)) {
        return false;
      } else {
        if (rowEl !== '.') {
          rows.add(rowEl);
        }
      }

      if (cols.has(colEl)) {
        return false;
      } else {
        if (colEl !== '.') {
          cols.add(colEl);
        }
      }

      if (boxes.has(boxEl)) {
        return false;
      } else {
        if (boxEl !== '.') {
          boxes.add(boxEl);
        }
      }
    }
    rows.clear();
    cols.clear();
    boxes.clear();
  }

  return true;
};

const isValidSudoku2 = function (board) {
  const n = 9;
  const rows = Array(n).fill('');
  const cols = Array(n).fill('');
  const boxes = Array(n).fill('');

  for (let r = 0; r < n; r++) {
    for (let c = 0; c < n; c++) {
      const value = board[r][c];

      if (value === '.') {
        continue;
      }

      if (rows[r].includes(value)) {
        return false;
      }
      rows[r] += value;

      if (cols[c].includes(value)) {
        return false;
      }
      cols[c] += value;

      const idx = Math.floor(r / 3) * 3 + Math.floor(c / 3);
      if (boxes[idx].includes(value)) {
        return false;
      }
      boxes[idx] += value;
    }
  }

  return true;
};

isValidSudoku([
  ["5", "3", ".", ".", "7", ".", ".", ".", "."]
  , ["6", ".", ".", "1", "9", "5", ".", ".", "."]
  , [".", "9", "8", ".", ".", ".", ".", "6", "."]
  , ["8", ".", ".", ".", "6", ".", ".", ".", "3"]
  , ["4", ".", ".", "8", ".", "3", ".", ".", "1"]
  , ["7", ".", ".", ".", "2", ".", ".", ".", "6"]
  , [".", "6", ".", ".", ".", ".", "2", "8", "."]
  , [".", ".", ".", "4", "1", "9", ".", ".", "5"]
  , [".", ".", ".", ".", "8", ".", ".", "7", "9"]
]);