/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var middleNode = function (head) {
  let counter = 1;
  let target = head;
  while (head.next) {
    head = head.next;
    counter++;
  }
  let steps = Math.floor(counter / 2);
  while (steps) {
    target = target.next;
    steps--;
  }
  return target;
};

var middleNode = function(head) {
  let slow, fast;
  slow = fast = head;
  while (fast && fast.next) {
      slow = slow.next;
      fast = fast.next.next;
  }
  return slow;
};