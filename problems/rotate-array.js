// Example 1:
// Input: nums = [1,2,3,4,5,6,7], k = 3
// Output: [5,6,7,1,2,3,4]

// Example 2:
// Input: nums = [-1,-100,3,99], k = 2
// Output: [3,99,-1,-100]

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
 var rotate = function(nums, k) {
    const cache = Array(nums.length);
    for (let i = 0; i < nums.length; i++) {
      cache[(i + k) % nums.length] = nums[i];
    }
    for (let i = 0; i < cache.length; i++) {
      nums[i] = cache[i];
    }
};